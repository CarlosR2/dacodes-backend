from flask_restful import Resource, reqparse
from model.userModel import UserModel
from model.courseModel import CourseModel
from model.lessonModel import LessonModel
from model.questionModel import  QuestionsModel
from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, jwt_refresh_token_required, get_jwt_identity, get_raw_jwt)
import json
from parsers import parser,parserC

from res.usersRes import *
from res.courseRes import *
from res.lessonRes import *
from res.questionRes import *


class AllLessons(Resource):
    def get(self):
       # print(get_jwt_identity())
        return LessonModel.return_all()



class SecretResource(Resource):
    def post(self):
        return {
            'answer': 42
        }