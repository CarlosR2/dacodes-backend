from flask_restful import Resource, reqparse
from model.courseModel import CourseModel
from model.questionModel import QuestionsModel
from model.lessonModel import LessonModel
from model.userModel import UserModel
from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, jwt_refresh_token_required, get_jwt_identity, get_raw_jwt)
import json
from parsers import *
from flask import request

class DeleteQuestion(Resource):
    @jwt_required
    def delete(self):
        current_user = UserModel.find_by_username(get_jwt_identity())
        if not current_user.isProfessor:
            return {'message': 'User don\'t have permission for this operation'}, 401
        data = parserQuestionDelete.parse_args()
        question = QuestionsModel.findById(data['id'])
        if not question:
            return {'message':'Question doesn\'t exist'}
        return QuestionsModel.deleteById(data['id'])    

class UpdateQuestion(Resource):
    @jwt_required
    def put(self):
        data = parserQuestionUpdate.parse_args()
        print(data)
        current_user = UserModel.find_by_username(get_jwt_identity())
        if not current_user.isProfessor:
            return {'message': 'User don\'t have permission for this operation'}, 401
        question = QuestionsModel.findById(data['id'])
        if not question:
            return {'message':'Question doesn\'t exist'}
        return QuestionsModel.updateById(data['id'],data['id_lesson'],data['quest'],data['score'],data['options'],data['answers'])
class GetQuestion(Resource):
    @jwt_required
    def get(self):
        try:
            args = request.args
            if not args['id']:
                return {'message':'Please provide question id'}
            question = QuestionsModel.findById(args['id'])
            current_user = UserModel.find_by_username(get_jwt_identity())
            if current_user.isProfessor:
                return {
                    'id':question.id,
                    'number':question.number,
                    'type':question.typeq,
                    'options':question.options,
                    'answers':question.answers,
                    'score':question.score
                }
            else:
                return {
                    'id':question.id,
                    'number':question.number,
                    'type':question.typeq,
                    'options':question.options,
                    'score':question.score
                }
        except:
            return {'message': 'Something went wrong'}, 500            

class CreateQuestion(Resource):
    @jwt_required
    def post(self):
        current_user = UserModel.find_by_username(get_jwt_identity())
        if not current_user.isProfessor:
            return {'message': 'User don\'t have permission for this operation'}, 401
        data = parserQuestion.parse_args()
        lesson = LessonModel.findById(data['id_lesson'])
        if not lesson:
            return {'message':'Lesson doesn\'t exist'}
        new_question = QuestionsModel(
            number = data['number'],
            id_lesson = data['id_lesson'],
            typeq = data['typeq'],
            quest = data['quest'],
            options = data['options'],
            answers = data['answers'],
            score = data['score']
        )
        try:
            new_question.save_to_db()
            return {
                'message': 'Question {} was created'.format(data['number']),
                }
        except Exception as e:
            print(e)
            return {'message': 'Something went wrong'}, 500            


