from flask_restful import Resource, reqparse
from model.courseModel import CourseModel
from model.lessonModel import LessonModel
from model.questionModel import QuestionsModel
from model.kardexLessonModel import KardexLessonModel
from model.kardexCourseModel import KardexCourseModel
from model.userModel import UserModel
from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, jwt_refresh_token_required, get_jwt_identity, get_raw_jwt)
import json
from parsers import *
from flask import request

class CreateLesson(Resource):
    @jwt_required
    def post(self):
        current_user = UserModel.find_by_username(get_jwt_identity())
        print(current_user.isProfessor)
        data = parserLessons.parse_args()
        if not current_user.isProfessor:
            return {'message': 'User don\'t have permission for this operation'}, 401
        if data['serial']:
            if not LessonModel.findById(data['serial']):
                return {'message':'Lesson doesn\'t exist'}    
        new_lesson = LessonModel(
            id =data['id'],
            name = data['name'],
            course_id = data['course_id'],
            score = data['score'],
            serial = data['serial']
        )
        try:
            new_lesson.save_to_db()
            return {
                'message': 'Lesson {} was created'.format(data['name']),
                }
        except Exception as e:
            print(e)
            return {'message': 'Something went wrong'}, 500           

class DeleteLesson(Resource):
    @jwt_required
    def delete(self):
        current_user = UserModel.find_by_username(get_jwt_identity())
        if not current_user.isProfessor:
            return {'message': 'User don\'t have permission for this operation'}, 401
        data = parserLessonsDelete.parse_args()
        lesson = LessonModel.findById(data['id'])
        if not lesson:
            return {'message':'Lesson doesn\'t exist'}
        return LessonModel.deleteById(data['id'])    

class UpdateLesson(Resource):
    @jwt_required
    def put(self):
        data = parserCoursesUpdate.parse_args()
        current_user = UserModel.find_by_username(get_jwt_identity())
        if not current_user.isProfessor:
            return {'message': 'User don\'t have permission for this operation'}, 401
        lesson = LessonModel.findById(data['id'])
        if not lesson:
            return {'message':'Course doesn\'t exist'}
        return LessonModel.updateById(data['id'],data['name'],data['course_id'],data['serial'])
class GetLesson(Resource):
    @jwt_required
    def get(self):
        try:
            args = request.args
            if not args['id']:
                return {'message':'Please provide lesson id'}
            lesson = LessonModel.findById(args['id'])
            return {
                'id':lesson.id,
                'name':lesson.name,
                'course_id':lesson.course_id,
                'score':lesson.score,
                'serial':lesson.serial
        } 
        except:
            return {'message': 'Something went wrong'}, 500      

class GetLessonQuestions(Resource):
    @jwt_required
    def get(self):
        try:
            args = request.args
            if not args['id']:
                return {'message':'Please provide lesson id'}
            lesson = LessonModel.findById(args['id'])
            if not lesson:
                return {'message':'Lesson doens\'t exists'}                
            return QuestionsModel.findByLessonId(args['id'])
        except Exception as e:
            print(e)
            return {'message': 'Something went wrong'}, 500 

class TakeLesson(Resource):
    @jwt_required
    def post(self):
        data = parsertLessonsAnswers.parse_args()
        current_user = UserModel.find_by_username(get_jwt_identity())
        if current_user.isProfessor :
            return {'message':'Only students can take a lesson'}
        lesson_id = data['lesson_id']
        lesson = LessonModel.findById(lesson_id)
        if not lesson:
            return {'message':'Lesson doens\'t exists'}
        print(type(lesson))
        answ = data['answ']
        print("validando leccion")
        if not TakeLesson.checkValid(current_user.id,lesson):
            return {'message': 'Something went wrong, please check yours previous lesson or courses'}, 500 
        questions = QuestionsModel.query.filter_by(id_lesson = lesson_id).all()
        print (answ)
        print(questions)
        json_acceptable_string = answ.replace("'", "\"")
        answ = json.loads(json_acceptable_string)
        print(type(answ))
        myscore = 0
        if isinstance(answ,dict):
            for element in questions:
                a = str(element.id)
                try:
                    res = answ[a]
                    try:
                        myscore += TakeLesson.checkAnswer(element,res) 
                        print(myscore)
                    except Exception as e:
                        print(e)
                except :
                    return {'message': 'Please answer all questions'}, 500 
            if myscore >= lesson.score:
                plesson = KardexLessonModel.query.filter(KardexLessonModel.lesson_id==lesson_id,KardexLessonModel.student_id==current_user.id).first()                    
                if not plesson:                
                    lessonRecord = KardexLessonModel(
                        student_id = current_user.id,
                        lesson_id = lesson_id,
                        score = myscore
                    )
                    lessonRecord.save_to_db()
                if TakeLesson.checkCourseComplete(current_user.id,lesson):
                    pcurse = KardexCourseModel.query.filter(KardexCourseModel.student_id==current_user.id,KardexCourseModel.course_id==lesson.course_id).first()
                    if not pcurse:
                        courseRecord = KardexCourseModel(
                            student_id=current_user.id,
                            course_id=lesson.course_id
                        )
                        courseRecord.save_to_db()
                return {'message': 'Congratulations',}
            return {'message': 'no aproval'}
        else :
            return {'message': 'Something went wrong, please provide the answers as array'}, 500 

    def checkAnswer(question,answ):
        if question.typeq == 1:
            op = question.answers.split(',')
            if op[0]==answ:
                return question.score
        elif question.typeq == 2:
            op = question.answers.split(',')
            if op[0]==answ:
                return question.score
        elif question.typeq == 3:
            op = question.answers.split(',')
            flag = False
            for element in op:
                if element == answ:
                    flag = True
                    break
            if flag:
                return question.score    
            print("tres")
            print(op)
        elif question.typeq == 4:
            op = question.answers.split(',')
            an = answ.split(',')
            print(op)
            print(an)
            flag = False
            r = list(set(op)-set(an))
            r1 = list(set(an)-set(op))     
            r=r+r1       
            if len(r) == 0:
                return question.score 
        return 0 

    def checkValid(id_student,mylesson):
        if mylesson.serial:
            blesson = KardexLessonModel.query.filter(KardexLessonModel.lesson_id==mylesson.serial,KardexLessonModel.student_id==id_student).first()
            if not blesson:
                return False
        mycourse = CourseModel.query.filter_by(id=mylesson.course_id).first()
        if mycourse.serial:
            bcourse = KardexCourseModel.query.filter(KardexCourseModel.course_id==mycourse.serial,KardexCourseModel.student_id==id_student).first()
            if not bcourse:
                return False
        return True
    def checkCourseComplete(id_student,mylesson):
        lessonsPassed = KardexLessonModel.query.filter_by(student_id=id_student).all()
        lessonsTotal = LessonModel.query.filter_by(course_id=mylesson.course_id).all()
        lessonTotmap = []
        lessonPassMap = []
        for element in lessonsTotal:
            lessonTotmap.append(element.id)
        for element in lessonsPassed:
            lessonPassMap.append(element.lesson_id)
        r = list(set(lessonTotmap)-set(lessonPassMap))
        if len(r) == 0:
            return True
        return False
class CreateQuestion(Resource):
    def post(self):
        data = parserQ.parse_args()
        new_question = QuestionsModel(
            number = data['number'],
            id_lesson = data['id_lesson'],
            typeq = data['typeq'],
            quest = data['quest'],
            options = data['options'],
            answers = data['answers'],
            score = data['score']
        )
        try:
            new_question.save_to_db()
            return {
                'message': 'Question {} was created'.format(data['number']),
                }
        except Exception as e:
            print(e)
            return {'message': 'Something went wrong'}, 500            


