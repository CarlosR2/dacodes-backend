from flask_restful import Resource, reqparse
from model.courseModel import CourseModel
from model.questionModel import QuestionsModel
from model.userModel import UserModel
from model.lessonModel import LessonModel
from model.kardexCourseModel import KardexCourseModel
from model.kardexLessonModel import KardexLessonModel
from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, jwt_refresh_token_required, get_jwt_identity, get_raw_jwt)
import json
from parsers import *
from flask import request

class CreateCourse(Resource):
    @jwt_required
    def post(self):
        current_user = UserModel.find_by_username(get_jwt_identity())
        print(current_user.isProfessor)
        data = parserCourses.parse_args()
        if not current_user.isProfessor:
            return {'message': 'User don\'t have permission for this operation'}, 401
        if data['serial']:
            if not CourseModel.findById(data['serial']):
                return {'message':'Course serial doesn\'t exist'}
        new_course = CourseModel(
            id =data['id'],
            name = data['name'],
            serial = data['serial']
        )
        try:
            new_course.save_to_db()
            return {
                'message': 'Course {} was created'.format(data['name']),
                }
        except Exception as e:
            print(e)
            return {'message': 'Something went wrong'}, 500           

class DeleteCourse(Resource):
    @jwt_required
    def delete(self):
        current_user = UserModel.find_by_username(get_jwt_identity())
        if not current_user.isProfessor:
            return {'message': 'User don\'t have permission for this operation'}, 401
        data = parserCoursesDelete.parse_args()
        course = CourseModel.findById(data['id'])
        if not course:
            return {'message':'Course doesn\'t exist'}
        return CourseModel.deleteCourseById(data['id'])    

class UpdateCourse(Resource):
    @jwt_required
    def put(self):
        data = parserCoursesUpdate.parse_args()
        current_user = UserModel.find_by_username(get_jwt_identity())
        if not current_user.isProfessor:
            return {'message': 'User don\'t have permission for this operation'}, 401
        course = CourseModel.findById(data['id'])
        if not course:
            return {'message':'Course doesn\'t exist'}
        return CourseModel.updateCourseById(data['id'],data['name'],data['serial'])
class GetCourse(Resource):
    @jwt_required
    def get(self):
        try:
            args = request.args
            if not args['id']:
                return {'message':'Please provide course id'}
            course = CourseModel.findById(args['id'])
            return {
                'id':course.id,
                'name':course.name,
                'serial':course.serial
        } 
        except:
            return {'message':'Please provide course id'}

class CreateQuestion(Resource):
    def post(self):
        data = parserQ.parse_args()
        new_question = QuestionsModel(
            number = data['number'],
            id_lesson = data['id_lesson'],
            typeq = data['typeq'],
            quest = data['quest'],
            options = data['options'],
            answers = data['answers'],
            score = data['score']
        )
        try:
            new_question.save_to_db()
            return {
                'message': 'Question {} was created'.format(data['number']),
                }
        except Exception as e:
            print(e)
            return {'message': 'Something went wrong'}, 500            

class GetAllCourse(Resource):
    @jwt_required
    def get(self):
        def to_json(x):
            if not x.serial:
                return {
                    'id': x.id,
                    'name': x.name,
                }
            else:
                return {
                    'id': x.id,
                    'name': x.name,
                    'serial': x.serial
                }   
        try:
            current_user = UserModel.find_by_username(get_jwt_identity())
            noSerial = list(map(lambda x: to_json(x), CourseModel.query.filter_by(serial = None).all()))
            serialized = list(map(lambda x: to_json(x), CourseModel.query.filter(CourseModel.serial != None).all()))
            if current_user.isProfessor:
                return {'NO SERIAL':noSerial,'SERIAL':serialized}
            else:
                coursesKardex = KardexCourseModel.query.filter_by(student_id=current_user.id).all()
                for element in coursesKardex:
                        for el in serialized:
                            if element.course_id == el['serial']:
                                noSerial.append(el)
                                serialized.remove(el)
                                print(el)
                print(coursesKardex)
                return {'Available':noSerial,'Not Available':serialized}
        except Exception as e:
            print(e)
            return {'message': 'Something went wrong'}, 500    

class CourseDesc(Resource):
    @jwt_required
    def get(self):
        def to_json(x):
            if not x.serial:
                return {
                    'id': x.id,
                    'name': x.name,
                }
            else:
                return {
                    'id': x.id,
                    'name': x.name,
                    'serial': x.serial
                }
        def kardex_to_json(x):
            return{
                'lesson_id':x.lesson_id,
                'score':x.score
            }     
        try:
                current_user = UserModel.find_by_username(get_jwt_identity())                
                args = request.args
                if not args['id']:
                    return {'message':'Please provide course id'}
                #lessonsTot = CourseModel.courseDescription(args['id'])['Lessons']
                serialized = list(map(lambda x: to_json(x), LessonModel.query.filter(LessonModel.serial != None,LessonModel.course_id == args['id']).all()))
                noserialized = list(map(lambda x: to_json(x), LessonModel.query.filter(LessonModel.serial == None,LessonModel.course_id == args['id']).all()))
                #noSerial = list(map(lambda x: to_json(x), LessonModel.query.filter(LessonModel.serial = None,LessonModel.course_id=args['id']).all()))
                #serialized = list(map(lambda x: to_json(x), LessonModel.query.filter(CourseModel.serial != None,LessonModel.course_id=args['id']).all()))
                if current_user.isProfessor:
                    return {'NO SERIAL':noserialized,'SERIAL':serialized}
                kardexLesson = list(map(lambda x: kardex_to_json(x),KardexLessonModel.query.filter_by(student_id = current_user.id).all()))
                print(noserialized)
                print(serialized)
                print(kardexLesson)
                for element in kardexLesson:
                    for el in serialized:
                        if element['lesson_id'] == el['serial']:
                            noserialized.append(el)
                            serialized.remove(el)
                return {'Aviable':noserialized,'Not aviable':serialized}




                #return CourseModel.courseDescription(args['id'])
        except Exception as e:
                print(e)
                return {'message': 'Something went wrong, Please provide a valid course id'}, 500    


