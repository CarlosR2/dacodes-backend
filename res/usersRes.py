from flask_restful import Resource, reqparse
from model.userModel import UserModel
from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, jwt_refresh_token_required, get_jwt_identity, get_raw_jwt)
import json
from parsers import parser,parserC 


class StudentRegistration(Resource):
    def post(self):
        data = parser.parse_args()
        a = UserModel.find_by_username(data['username'])
        print (a)
        if UserModel.find_by_username(data['username']):
            return {'message': 'User {} already exists'. format(data['username'])}
        new_user = UserModel(
                username = data['username'],
                password = UserModel.generate_hash(data['password']),
                isProfessor = False
            )
        try:
            new_user.save_to_db()    
            return {
                    'message': 'User {} was created'.format(data['username']),
                    }
        except Exception as e:
            print(e)
            return {'message': 'Something went wrong'}, 500

class ProfessorRegistration(Resource):
    def post(self):
        data = parser.parse_args()
        if UserModel.find_by_username(data['username']):
            return {'message': 'User {} already exists'. format(data['username'])}
        new_user = UserModel(
            username = data['username'],
            password = UserModel.generate_hash(data['password']),
            isProfessor = True
        )
        try:
            new_user.save_to_db()
            return {
                'message': 'User {} was created'.format(data['username']),
                }
        except:
            return {'message': 'Something went wrong'}, 500
            
class UserLogin(Resource):
    def post(self):
        data = parser.parse_args()
        current_user = UserModel.find_by_username(data['username'])
        if not current_user:
            return {'message': 'User {} doesn\'t exist'. format(data['username'])}
        if UserModel.verify_hash(data['password'],current_user.password):
            access_token = create_access_token(identity = data['username'])
            refresh_token = create_refresh_token(identity = data['username'])
            role = 'Professor' if current_user.isProfessor==1 else 'Student'
            return {
                'message': 'Logged in as {}'.format(current_user.username),
                'role':role,
                'access_token': access_token
                }
        else:
            return {'message': 'Wrong credentials'}
      
      
      
class AllUsers(Resource):
    @jwt_required
    def get(self):
        print(get_jwt_identity())
        return UserModel.return_all()
    @jwt_required
    def delete(self):
        return UserModel.delete_all()
