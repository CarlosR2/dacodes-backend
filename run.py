from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager

app = Flask(__name__)
api = Api(app)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///app.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = 'asdlkjasdlkj'

db = SQLAlchemy(app)

#@app.before_first_request
#def create_tables():
#    db.create_all()

app.config['JWT_SECRET_KEY'] = 'aosjdasjdasdlkjasdlkasd'
jwt = JWTManager(app)

import views, models, resources

api.add_resource(resources.StudentRegistration, '/createStudent')
api.add_resource(resources.ProfessorRegistration, '/createProfessor')
api.add_resource(resources.UserLogin, '/login')


api.add_resource(resources.CreateCourse,'/course')
api.add_resource(resources.DeleteCourse,'/course')
api.add_resource(resources.UpdateCourse,'/course')
api.add_resource(resources.GetCourse,'/course')
api.add_resource(resources.GetAllCourse,'/allCourse')


api.add_resource(resources.CreateLesson,'/lesson')
api.add_resource(resources.DeleteLesson,'/lesson')
api.add_resource(resources.UpdateLesson,'/lesson')
api.add_resource(resources.GetLesson,'/lesson')

api.add_resource(resources.CreateQuestion,'/questions')
api.add_resource(resources.DeleteQuestion,'/questions')
api.add_resource(resources.UpdateQuestion,'/questions')
api.add_resource(resources.GetQuestion,'/questions')

api.add_resource(resources.GetLessonQuestions,'/lessonQuestions')
api.add_resource(resources.TakeLesson,'/lessonQuestions')

api.add_resource(resources.AllUsers, '/users')
api.add_resource(resources.CourseDesc,'/courseDesc')
api.add_resource(resources.AllLessons, '/lessons')

#api.add_resource(resources.SecretResource, '/secret')
