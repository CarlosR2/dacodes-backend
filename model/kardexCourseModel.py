from run import db

class KardexCourseModel(db.Model):
    
    __tablename__ = 'kardex_courses'
    id = db.Column(db.Integer, primary_key = True)
    student_id = db.Column(db.Integer, unique = True, nullable = False)
    course_id = db.Column(db.Integer, nullable = False)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
    @classmethod
    def findById(cls,id):
        return cls.query.filter_by(id = id).first()
    @classmethod
    def deleteById(cls,id):
        try:
            lesson = cls.query.filter_by(id = id).delete()
            db.session.commit()
            return {'message': 'Lesson with id {} deleted'.format(id)}
        except Exception as e:
            return {'message': 'Something went wrong'}
    @classmethod        
    def return_all(cls):
        def to_json(x):
            return {
                'id': x.id,
                'name': x.name,
                'course_id': x.course_id,
                'score':x.score
            }
        return {'Lessons': list(map(lambda x: to_json(x), LessonModel.query.all()))}

    @classmethod        
    def byCourse(cls,course_id):
        def to_json(x):
            return {
                'id': x.id,
                'name': x.name
            }
        return {'Lessons': list(map(lambda x: to_json(x), LessonModel.query.filter_by(course_id = course_id).all()))}    
    
    
   