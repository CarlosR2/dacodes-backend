from run import db

class QuestionsModel(db.Model):
    __tablename__ = 'questions'

    #id = db.Column(db.Integer, primary_key = True)
    #username = db.Column(db.String(120), unique = True, nullable = False)
    #password = db.Column(db.String(120), nullable = False)
    #isProfessor = db.Column(db.Boolean,nullable = False)
    

    

    #__tablename__ = 'questions'
    id = db.Column(db.Integer, primary_key = True)
    number = db.Column(db.Integer, nullable = False)
    id_lesson = db.Column(db.Integer, nullable = False)
    typeq = db.Column(db.Integer, nullable = False)
    quest = db.Column(db.String(250), nullable = False)
    options = db.Column(db.String(250), nullable = False)
    answers = db.Column(db.String(250), nullable = False)
    score = db.Column(db.Integer, nullable = False)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def findById(cls,id):
        return cls.query.filter_by(id = id).first()
    @classmethod
    def findByLessonId(cls,id_lesson):
        def to_json(x):
            return {
                'id': x.id,
                'number': x.number,
                'typeq': x.typeq,
                'score':x.score,
                'question':x.quest,
                'options':x.options
            }
        return {'Questions': list(map(lambda x: to_json(x), cls.query.filter_by(id_lesson = id_lesson).all()))}        
    @classmethod
    def deleteById(cls,id):
        try:
            question = cls.query.filter_by(id = id).delete()
            db.session.commit()
            return {'message': 'Question with id {} deleted'.format(id)}
        except Exception as e:
            print(e)
            return {'message': 'Something went wrong'}
    @classmethod        
    def return_all(cls):
        def to_json(x):
            return {
                'id': x.id,
                'number': x.name,
                'typeq': x.course_id,
                'score':x.score,
                'question':x.quest,
                'options':x.options
            }
        return {'Lessons': list(map(lambda x: to_json(x), LessonModel.query.filter_by(course_id = course_id).all()))}
    @classmethod
    def updateById(cls,id,id_lesson,quest,score,options,answers):
        try:
            question = cls.query.filter_by(id = id).first()
            if id_lesson:
                question.id_lesson = id_lesson
            if quest:
                question.quest = quest
            if score:
                question.score = score
            if options:
                question.options = options
            if answers:
                question.answers = answers
            db.session.commit()
            return {'message': 'Question with id:{} was update successfully'.format(id)}
        except Exception as e:
            print(e)
            return {'message': 'Something went wrong'}  