from run import db

class KardexLessonModel(db.Model):
    
    __tablename__ = 'kardex_lessons'
    id = db.Column(db.Integer, primary_key = True)
    student_id = db.Column(db.Integer, nullable = False)
    lesson_id = db.Column(db.Integer, nullable = False)
    score = db.Column(db.Integer, nullable = False)


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
    @classmethod
    def findById(cls,id):
        return cls.query.filter_by(id = id).first()
    @classmethod
    def findByLesson_Id(cls,lesson_id):
        return cls.query.filter_by(lesson_id = lesson_id).first()        


    @classmethod        
    def return_all(cls):
        def to_json(x):
            return {
                'id': x.id,
                'name': x.name,
                'course_id': x.course_id,
                'score':x.score
            }
        return {'Lessons': list(map(lambda x: to_json(x), LessonModel.query.all()))}

    @classmethod
    def updateById(cls,id,name,course_id,score,serial):
        try:
            lesson = cls.query.filter_by(id = id).first()
            if name:
                lesson.name = name
            if course_id:
                lesson.course_id = course_id
            if score:
                lesson.score = score
            if serial:
                if int(serial) < 0:
                    lesson.serial = None
                else:
                    if not LessonModel.findById(serial):
                        return {'message': 'Lesson with id:{} doesn\'t exists' .format(serial)}
                    lesson.serial = serial
            db.session.commit()
            return {'message': 'Lesson with id:{} was update successfully'.format(id)}
        except Exception as e:
            print(e)
            return {'message': 'Something went wrong'}  

    @classmethod        
    def byCourse(cls,course_id):
        def to_json(x):
            return {
                'id': x.id,
                'name': x.name
            }
        return {'Lessons': list(map(lambda x: to_json(x), LessonModel.query.filter_by(course_id = course_id).all()))}    
    
    
   