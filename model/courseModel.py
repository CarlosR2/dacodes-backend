
from run import db
from model.lessonModel import LessonModel

class CourseModel(db.Model):
    __tablename__ = 'courses'

    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(120), unique = True, nullable = False)
    serial = db.Column(db.Integer, nullable = False)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
    @classmethod
    def courseDescription(cls,id):
        course = cls.query.filter_by(id = id).first()
        return LessonModel.byCourse(course.id)
    @classmethod
    def findById(cls,id):
        return cls.query.filter_by(id = id).first()
    @classmethod
    def deleteCourseById(cls,id):
        try:
            course = cls.query.filter_by(id = id).delete()
            db.session.commit()
            return {'message': 'Course {} deleted'.format(id)}
        except Exception as e:
            return {'message': 'Something went wrong'}

    @classmethod
    def updateCourseById(cls,id,name,serial):
        try:
            course = cls.query.filter_by(id = id).first()
            if name:
                course.name = name
            print(serial)
            if serial:
                if int(serial) < 0:
                    course.serial = None
                else:
                    if not CourseModel.findById(serial):
                        return {'message': 'Course with id:{} doesn\'t exists' .format(serial)}
                    course.serial = serial
            db.session.commit()
            return {'message': 'Course with id:{} was updated successfully'.format(id)}
        except Exception as e:
            print(e)
            return {'message': 'Something went wrong'}  