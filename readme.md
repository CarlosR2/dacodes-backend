# Dacodes back-end test

## Install and run

### Requeriments

1. Python3
2. virtualenv 


### Install virtualenv

```bash
$ sudo apt-get install python-virtualenv virtualenv
```
### Clone repository

```bash
$ git clone https://gitlab.com/CarlosR2/dacodes-backend.git
```
### Move to the project dir

```bash
$ cd dacodes-backend/
```
### Create virtual enviroment
```bash
$ virtualenv env --python=python3
```
### Start virtual enviroment
```bash
$ source env/bin/activate
```
### Install flask
```bash
(env)$ pip install flask flask-restful flask-jwt-extended passlib flask-sqlalchemy
```
### Run project
```bash
$ FLASK_APP=run.py FLASK_DEBUG=1 flask run
```
### If you get an error for LANG type the follow line and run again

```bash
$ export LANG=C.UTF-8

$ FLASK_APP=run.py FLASK_DEBUG=1 flask run

```

 
# API DOC

## Create users

### There are two kinds of users **Students** and **Professor**

### **URL**  localhost:5000/createStudent

### **Method** POST
### **Body** 
```
{
    "username":"myusername",
    "password":"password"
}
```
### **URL**  localhost:5000/createProfessor
### **Method** POST
### **Body** 
```
{
    "username":"myusername",
    "password":"password"
}
```

## User Login

### **URL**  localhost:5000/login
### **Method** POST
### **Body** 
```
{
    "username":"myusername",
    "password":"password"
}
```
### **Response** 

```
{
    "message": "Logged in as st1",
    "role": "Student",
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1Njg5NDE4OTksIm5iZiI6MTU2ODk0MTg5OSwianRpIjoiZGYwNzdlZDAtYjRmMS00YTVhLTk5MDktOTM0OTVjZTRmOWU5IiwiZXhwIjoxNTY4OTQyNzk5LCJpZGVudGl0eSI6InN0MSIsImZyZXNoIjpmYWxzZSwidHlwZSI6ImFjY2VzcyJ9.wfHsNkq4KfBbaPR59_9DZg9SsQc7q3Pow6dZfC3QiNU"
}
```
### Test users
| Id     | User | Pass | Role      |
|------- | ---- |:----:| ---------:|
| 1      | pf1  | 1234 | Professor |
| 2      | pf2  | 1234 | Professor |
| 3      | st1  | 1234 | Student   |
| 4      | st2  | 1234 | Student   |
## Courses

#### Only user professor can create, delete or update a course

## Create

### **URL** localhost:5000/course

### **Method** POST
### **AUTH** Bearer token
### **Body** 
```
{
	"id": 1,
	"name":"Curso Extra Final",
	"serial":2 (OPTIONAL)
}
```
## Delete

### **URL** localhost:5000/course

### **Method** Delete
### **AUTH** Bearer token
### **Body** 
```
{
	"id": 1,
}
```
## Update

### **URL** localhost:5000/course

### **Method** PUT
### **AUTH** Bearer token
### **Body** 
```
{
	"id": 1,
	"name":"Curso Extra Modificado", (OPTIONAL)
	"serial":2 (OPTIONAL) // -1 to delete serial
}
```

## Get specific course

### **URL** localhost:5000/course?id=_id

### **Method** Get
### **AUTH** Bearer token
### **Example** 
```
GET

localhost:5000/course?id=2

Response:

{
    "id": 2,
    "name": "Curso Base 2",
    "serial": 1 (Null if it isn't serialized)
}

```

### Get All courses

#### Professors can see Serial and Not Serial Courses
#### Students can see their courses Available to take and Not Available

### **URL** localhost:5000/allCourse
### **Method** Get
### **AUTH** Bearer token
### **Example Student** 
```
GET

localhost:5000/allCourse

Response:

{
    "Available": [
        {
            "id": 1,
            "name": "Curso Base 1"
        },
        {
            "id": 4,
            "name": "Curso Extra 1"
        }
    ],
    "Not Available": [
        {
            "id": 2,
            "name": "Curso Base 2",
            "serial": 1
        },
        {
            "id": 3,
            "name": "Curso Base 3",
            "serial": 2
        },
        {
            "id": 7,
            "name": "Curso Extra Final",
            "serial": 2
        }
    ]
}

```

### **Example Professor** 
```
GET

localhost:5000/allCourse

Response:

{
    "NO SERIAL": [
        {
            "id": 1,
            "name": "Curso Base 1"
        },
        {
            "id": 4,
            "name": "Curso Extra 1"
        }
    ],
    "SERIAL": [
        {
            "id": 2,
            "name": "Curso Base 2",
            "serial": 1
        },
        {
            "id": 3,
            "name": "Curso Base 3",
            "serial": 2
        },
        {
            "id": 7,
            "name": "Curso Extra Final",
            "serial": 2
        }
    ]
}
```

## Get course's lessons available

### **URL** localhost:5000/courseDesc?id=_id

### **Method** Get
### **AUTH** Bearer token
### **Example Student** 
```
GET

localhost:5000/courseDesc?id=1

Response:

{
    "Aviable": [
        {
            "id": 10,
            "name": "leccion A"
        },
        {
            "id": 13,
            "name": "leccion D"
        }
    ],
    "Not aviable": [
        {
            "id": 11,
            "name": "leccion B",
            "serial": 10
        },
        {
            "id": 12,
            "name": "leccion C",
            "serial": 11
        }
    ]
}


```

### **Example Professor** 
```
GET

localhost:5000/courseDesc?id=1

Response:
{
    "NO SERIAL": [
        {
            "id": 10,
            "name": "leccion A"
        },
        {
            "id": 13,
            "name": "leccion D"
        }
    ],
    "SERIAL": [
        {
            "id": 11,
            "name": "leccion B",
            "serial": 10
        },
        {
            "id": 12,
            "name": "leccion C",
            "serial": 11
        }
    ]
}
```





## Lessons

#### Only user professor can create, delete or update a course

## Create

### **URL** localhost:5000/lesson

### **Method** POST
### **AUTH** Bearer token
### **Body** 
```
{
	"id":32,
	"name":"Leccion F",
	"course_id":5,
	"score":10,
	"serial":31 (Optional)
}
```
## Delete

### **URL** localhost:5000/lesson

### **Method** Delete
### **AUTH** Bearer token
### **Body** 
```
{
	"id": 32,
}
```
## Update

### **URL** localhost:5000/lesson

### **Method** PUT
### **AUTH** Bearer token
### **Body** 
```
{
	"id":32,
	"name":"Leccion F",(Optional)
	"course_id":5,(Optional)
	"score":10,(Optional)
	"serial":31(Optional)
}
```

## Get specific lesson

### **URL** localhost:5000/lesson?id=_id

### **Method** Get
### **AUTH** Bearer token
### **Example** 
```
GET

localhost:5000/lesson?id=10
Response:

{
    "id": 10,
    "name": "leccion A",
    "course_id": 1,
    "score": 5,
    "serial": null
}

```



## Get Lesson's questions

### **URL** localhost:5000/lessonQuestions?id=_id

### **Method** Get
### **AUTH** Bearer token
```
GET

localhost:5000/lessonQuestions?id=11

Response:

{
    "Questions": [
        {
            "id": 3,
            "number": 1,
            "typeq": 3,
            "score": 5,
            "question": "Selecciona un pais",
            "options": "mexico,usa,yucatan"
        },
        {
            "id": 4,
            "number": 2,
            "typeq": 4,
            "score": 5,
            "question": "Selecciona todos los paises",
            "options": "mexico,usa,yucatan"
        }
    ]
}

```

## Take a lesson

#### Only Students can take a lesson

### **URL** localhost:5000/lessonQuestions

### **Method** POST
### **AUTH** Bearer token
```
{
	"lesson_id":"12",
	"answ":{"19":"usa","20":"usa,mexico"}
	
}

```
## **Important!**

There are four types of questions
 - Type 1: True/False  
        
        Example: (id:10)
        
        ¿EL cielo es azul?
        
        Options: True,False

 - Type 2: Multiple choice, only one answer is correct
        
        Example: (id:20)
        EL cielo es de color...?
        
        Options: azul,rojo,amarrillo
        

  - Type 3: Multiple choice, multiple answers are correct    
            
        Example: (id:30)
        Selecciona un pais

        Options: mexico,usa,yucatan

  - Type 4: Multiple choice, multiple answers are correct and all of them must be answered correctly 

        Example: (id:2):
        Selecciona todos los paises

        Options: mexico,usa,yucatan

The format for take a lesson is describe below:

- "lesson_id": It's the id for the lesson to take
- "answ": It's a json with a id question and answer:
    
        {
	    "lesson_id":"99",
	    "answ":{
            "10":"True", 
            "20":"azul",
            "30":"mexico",
            "40":"usa,mexico"
            }
        }
   

## Questions

#### Only user professor can create, delete or update a question

## Create

### **URL** localhost:5000/questions

### **Method** POST
### **AUTH** Bearer token
### **Body** 
```
{
	"number": 14,
	"id_lesson":13,
	"typeq":3,
	"quest":"Selecciona un pais?",
	"options":"mexico,usa,yucatan",
	"answers":"mexico,usa",
	"score":5
}
```
## Delete

### **URL** localhost:5000/questions

### **Method** Delete
### **AUTH** Bearer token
### **Body** 
```
{
	"id": 20,
}
```
## Update

### **URL** localhost:5000/questions

### **Method** PUT
### **AUTH** Bearer token
### **Body** 
```
{
	"id": 19,
	"id_lesson":13,
	"quest":"Selecciona un pais porfavor",
	"options":"mexico,usa,yucatan,america",
	"answers":"mexico,usa",
	"score":5
}
```







