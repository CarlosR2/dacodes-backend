from flask_restful import  reqparse


parser = reqparse.RequestParser()
parser.add_argument('username', help = 'This field cannot be blank', required = True)
parser.add_argument('password', help = 'This field cannot be blank', required = True)


parserC = reqparse.RequestParser()
parserC.add_argument('id', help = 'This field cannot be blank', required = True)

parserQuestion = reqparse.RequestParser()
parserQuestion.add_argument('number', help = 'This field cannot be blank', required = True)
parserQuestion.add_argument('id_lesson', help = 'This field cannot be blank', required = True)
parserQuestion.add_argument('typeq', help = 'This field cannot be blank', required = True)
parserQuestion.add_argument('quest', help = 'This field cannot be blank', required = True)
parserQuestion.add_argument('options', help = 'This field cannot be blank', required = True)
parserQuestion.add_argument('answers', help = 'This field cannot be blank', required = True)
parserQuestion.add_argument('score', help = 'This field cannot be blank', required = True)

parserQuestionDelete = reqparse.RequestParser()
parserQuestionDelete.add_argument('id', help = 'This field cannot be blank', required = True)

parserQuestionUpdate = reqparse.RequestParser()
parserQuestionUpdate.add_argument('id', help = 'This field cannot be blank', required = True)
parserQuestionUpdate.add_argument('id_lesson', help = 'This field cannot be blank', required = False)
parserQuestionUpdate.add_argument('quest', help = 'This field cannot be blank', required = False)
parserQuestionUpdate.add_argument('options', help = 'This field cannot be blank', required = False)
parserQuestionUpdate.add_argument('answers', help = 'This field cannot be blank', required = False)
parserQuestionUpdate.add_argument('score', help = 'This field cannot be blank', required = False)

parserCourses = reqparse.RequestParser()
parserCourses.add_argument('id', help = 'This field cannot be blank', required = True)
parserCourses.add_argument('name', help = 'This field cannot be blank', required = True)
parserCourses.add_argument('serial', help = '', required = False)

parserCoursesDelete = reqparse.RequestParser()
parserCoursesDelete.add_argument('id', help = 'This field cannot be blank', required = True)

parserCoursesUpdate = reqparse.RequestParser()
parserCoursesUpdate.add_argument('id', help = 'This field cannot be blank', required = True)
parserCoursesUpdate.add_argument('name', help = 'This field cannot be blank', required = False)
parserCoursesUpdate.add_argument('serial', help = '', required = False)


parserLessons = reqparse.RequestParser()
parserLessons.add_argument('id', help = 'This field cannot be blank', required = True)
parserLessons.add_argument('name', help = 'This field cannot be blank', required = True)
parserLessons.add_argument('course_id', help = 'This field cannot be blank', required = True)
parserLessons.add_argument('score', help = 'This field cannot be blank', required = True)
parserLessons.add_argument('serial', help = '', required = False)


parserLessonsDelete = reqparse.RequestParser()
parserLessonsDelete.add_argument('id', help = 'This field cannot be blank', required = True)

parserLessonsUpdate = reqparse.RequestParser()
parserLessonsUpdate.add_argument('id', help = 'This field cannot be blank', required = True)
parserLessonsUpdate.add_argument('name', help = 'This field cannot be blank', required = False)
parserLessonsUpdate.add_argument('course_id', help = 'This field cannot be blank', required = False)
parserLessonsUpdate.add_argument('score', help = 'This field cannot be blank', required = False)
parserLessonsUpdate.add_argument('serial', help = '', required = False)


parsertLessonsAnswers = reqparse.RequestParser()
parsertLessonsAnswers.add_argument('lesson_id',help = 'This field cannot be blank', required = True)
parsertLessonsAnswers.add_argument('answ',help = 'This field cannot be blank', required = True)